package br.com.frach.bolsa.command;

import br.com.frach.bolsa.Base;
import br.com.frach.bolsa.data.User;
import br.com.frach.bolsa.data.UserCaptcha;
import br.com.frach.bolsa.inventory.CaptchaInventory;
import br.com.frach.bolsa.inventory.SellInventory;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Optional;

public class SellCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if(!(commandSender instanceof Player)){
            commandSender.sendMessage("§cComando apenas para jogadores.");
            return true;
        }
        Player player = (Player) commandSender;

        User user = Base.userController.get(player, true);
        Optional<UserCaptcha> optionalUserCaptcha = Base.captchaController.getService().get(user);

        if(optionalUserCaptcha.isPresent()){
            UserCaptcha userCaptcha = optionalUserCaptcha.get();
            if(userCaptcha.isRequire()){
                CaptchaInventory.open(player);
            } else {
                SellInventory.open(player);
            }
            return true;
        }

        SellInventory.open(player);

        return false;
    }

}