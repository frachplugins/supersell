package br.com.frach.bolsa.data;

import org.bukkit.inventory.ItemStack;

public class ShopItem {

    private ItemStack item;
    private double price;

    public ShopItem(ItemStack item, double price){
        this.item = item;
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public ItemStack getItem() {
        return item;
    }

}