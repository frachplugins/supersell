package br.com.frach.bolsa.data;

import org.bukkit.inventory.ItemStack;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class Shop {

    private String name;
    private List<ShopItem> itens;
    private List<String> aliases;

    public Shop(String name, List<ShopItem> itens, List<String> aliases){
        this.name = name;
        this.itens = itens;
        this.aliases = aliases;
    }

    public List<String> getAliases() {
        return aliases;
    }

    public String getName() {
        return name;
    }

    public List<ShopItem> getItens() {
        return itens;
    }

    public Optional<Double> getPrice(ItemStack item){
        if(item == null){
            return Optional.empty();
        }
        return itens.stream().filter(shopItem -> shopItem.getItem().isSimilar(item)).max(Comparator.comparingDouble(ShopItem::getPrice)).map(ShopItem::getPrice);
    }

}