package br.com.frach.bolsa.data;

import org.bukkit.inventory.ItemStack;

public class SellItem extends ShopItem {

    private int amount;

    public SellItem(ItemStack itemStack, double price, int amount) {
        super(itemStack, price);
        this.amount = amount;
    }

    public int getAmount() {
        return amount;
    }

    public double getTotalPrice(){
        double perItem = getPrice() / 2304;
        return perItem * getAmount();
    }

}