package br.com.frach.bolsa.data;

import br.com.frach.bolsa.Base;
import br.com.frach.bolsa.utils.CaptchaUtil;

public class UserCaptcha {

    private User user;
    private CaptchaUtil.CaptchaItem material;
    private boolean require;
    private boolean inCaptcha;

    private long countdown;

    public UserCaptcha(User user) {
        this.user = user;
        this.require = true;
    }

    public boolean isInCaptcha() {
        return inCaptcha;
    }

    public void setInCaptcha(boolean inCaptcha) {
        this.inCaptcha = inCaptcha;
    }

    public CaptchaUtil.CaptchaItem getMaterial() {
        return material;
    }

    public void setMaterial(CaptchaUtil.CaptchaItem material) {
        this.material = material;
    }

    public boolean isRequire() {
        return require;
    }

    public void setRequire(boolean require) {
        this.require = require;
        if(!require){
            this.countdown = System.currentTimeMillis();
        }
    }

    public User getUser() {
        return user;
    }

    public void run(){
        if(!isRequire()) {
            int time = (int) ((System.currentTimeMillis() - this.countdown) / 1000) * 60;
            if (time >= Base.config.getInt("Captcha.Delay")) {
                this.setRequire(true);
            }
        }
    }

}