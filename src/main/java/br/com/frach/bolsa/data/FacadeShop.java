package br.com.frach.bolsa.data;

import br.com.frach.bolsa.Base;

import java.util.Comparator;
import java.util.List;

public class FacadeShop {

    private User user;
    private List<SellItem> itens;
    private Shop shop;

    public FacadeShop(User user, Shop shop, List<SellItem> itens){
        this.itens = itens;
        this.user = user;
        this.shop = shop;
    }

    public int getItemCount(){
        return itens.stream().mapToInt(SellItem::getAmount).sum();
    }

    public double getTotalClean(){
        return itens.stream().mapToDouble(SellItem::getTotalPrice).sum();
    }

    public double getTotal(){
        ShopMultiplier multiplier = Base.shopController.getService().getMultipliers().stream().filter(m ->
            user.getPlayer().hasPermission("plasmabolsa.multiplier." + m.getName())
        ).sorted(Comparator.comparingDouble(ShopMultiplier::getMultiplier)).findFirst().orElse(null);

        double fakeResult = getTotalClean() * ((multiplier == null ? 1 : multiplier.getMultiplier()));

        double result = Base.bolsaController.apply(fakeResult);

        return result;
    }

}