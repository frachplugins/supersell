package br.com.frach.bolsa.data;

public class ShopMultiplier {

    private String name;
    private double multiplier;

    public ShopMultiplier(String name, double multiplier){
        this.name = name;
        this.multiplier = multiplier;
    }

    public double getMultiplier() {
        return multiplier;
    }

    public String getName() {
        return name;
    }

}