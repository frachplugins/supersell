package br.com.frach.bolsa.data;

import br.com.frachdev.PlayerRank;
import br.com.frachdev.RankAPI;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class User {

    private Player player;
    private boolean shiftSell;
    private boolean autoSell;

    public User(Player player){
        this.player = player;
        this.shiftSell = false;
        this.autoSell = false;
    }

    public Player getPlayer() {
        return player;
    }

    public String getGroup(){
        PlayerRank group = RankAPI.Companion.getRank(player);
        System.out.println(group.getRank().getName());
        return group == null ? null : group.getRank().getName();
    }

    public void setAutoSell(boolean autoSell) {
        this.autoSell = autoSell;
    }

    public void setShiftSell(boolean shiftSell) {
        this.shiftSell = shiftSell;
    }

    public void toggleShiftSell(){
        this.shiftSell = !isShiftSell();
    }

    public void toggleAutoSell(){
        this.autoSell = !isAutoSell();
    }

    public boolean isShiftSell() {
        return shiftSell;
    }

    public boolean isAutoSell() {
        return autoSell;
    }

    public int getEmptySlotsCount() {
        int count = 0;
        ItemStack[] invContents = getPlayer().getInventory().getContents();

        for (final ItemStack item : invContents) {
            if (item == null) count++;
        }

        return count;
    }

}