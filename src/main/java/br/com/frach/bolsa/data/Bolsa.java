package br.com.frach.bolsa.data;

import br.com.frach.bolsa.Base;
import br.com.frach.bolsa.utils.MessageBuilder;
import org.bukkit.Bukkit;

import java.util.Random;

public class Bolsa {

    private String name;
    private int percent;
    private int maxPercent;
    private int delay;

    public Bolsa(String name, int maxPercent, int delay) {
        this.name = name;
        this.maxPercent = maxPercent;
        this.delay = delay;
    }

    public int getDelay() {
        return delay;
    }

    public String getName() {
        return name;
    }

    public int getPercent() {
        return percent;
    }

    public void update(){
        this.percent = new Random().nextInt(maxPercent);

        MessageBuilder builder = new MessageBuilder(Base.mensagens.getString("Mensagens.BOLSA_UPDATE"));
        builder.add("${percent}", "" + getPercent());

        MessageBuilder title = new MessageBuilder(Base.mensagens.getString("Title.BOLSA_UPDATE.First"));
        title.add("${percent}", "" + getPercent());

        MessageBuilder subTitle = new MessageBuilder(Base.mensagens.getString("Title.BOLSA_UPDATE.Sub"));
        subTitle.add("${percent}", "" + getPercent());

        Bukkit.getOnlinePlayers().forEach(p -> {
            p.sendMessage(builder.build());

            p.sendTitle(title.build(), subTitle.build());
        });
    }

}