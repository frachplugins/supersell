package br.com.frach.bolsa;

import br.com.frach.bolsa.command.SellCommand;
import br.com.frach.bolsa.controller.BolsaController;
import br.com.frach.bolsa.controller.CaptchaController;
import br.com.frach.bolsa.controller.ShopController;
import br.com.frach.bolsa.controller.UserController;
import br.com.frach.bolsa.data.User;
import br.com.frach.bolsa.listeners.UserListener;
import br.com.frach.bolsa.service.*;
import br.com.frach.bolsa.utils.PiranhaConfig;
import br.com.frach.bolsa.utils.inventory.InventoryManager;
import com.google.common.collect.Lists;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;

public class Base extends JavaPlugin {

    public static PiranhaConfig bolsas, config, gui, mensagens;

    private List<Service> services = Lists.newArrayList();

    public static ShopController shopController;
    public static UserController userController;
    public static BolsaController bolsaController;
    public static CaptchaController captchaController;

    public static InventoryManager inventoryManager;

    public Base(){

        ShopService shopService = new ShopService();
        shopController = new ShopController(shopService);
        services.add(shopService);

        CaptchaService captchaService = new CaptchaService();
        captchaController = new CaptchaController(captchaService);
        services.add(captchaService);

        UserService userService = new UserService();
        userController = new UserController(userService);
        services.add(userService);

        BolsaService bolsaService = new BolsaService();
        bolsaController = new BolsaController(bolsaService);
        services.add(bolsaService);

    }

    @Override
    public void onEnable() {

        getDataFolder().mkdirs();
        config = new PiranhaConfig(this, "config");
        bolsas = new PiranhaConfig(this, "bolsa");
        gui = new PiranhaConfig(this, "gui");
        mensagens = new PiranhaConfig(this, "mensagens");

        inventoryManager = new InventoryManager(this);
        inventoryManager.init();

        services.forEach(Service::init);

        getCommand("vender").setExecutor(new SellCommand());
        getServer().getPluginManager().registerEvents(new UserListener(), this);

        new BukkitRunnable(){
            @Override
            public void run() {
                userController.getService().all().stream().filter(User::isAutoSell).forEach(user ->
                        shopController.sell(user)
                );
                captchaController.getService().run();
            }
        }.runTaskTimer(this, 20L, 20L * 3);
    }

    @Override
    public void onDisable() {
        services.forEach(Service::stop);
    }

}