package br.com.frach.bolsa.listeners;

import br.com.frach.bolsa.Base;
import br.com.frach.bolsa.data.User;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerToggleSneakEvent;

public class UserListener implements Listener {

    @EventHandler
    public void shift(PlayerToggleSneakEvent e){
        Player player = e.getPlayer();
        final User user = Base.userController.get(player, true);
        if(user.isShiftSell() && player.isSneaking()){
            Base.shopController.sell(user);
        }
        Base.captchaController.build(user);
    }

}
