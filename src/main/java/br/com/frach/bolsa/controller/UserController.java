package br.com.frach.bolsa.controller;

import br.com.frach.bolsa.Base;
import br.com.frach.bolsa.data.User;
import br.com.frach.bolsa.service.UserService;
import org.bukkit.entity.Player;

import java.util.Optional;

public class UserController {

    private UserService service;

    public UserController(UserService service){
        this.service = service;
    }

    public User get(Player player, boolean autoCreate){
        Optional<User> optionalUser = service.get(player);
        if(optionalUser.isPresent()){
            return optionalUser.get();
        } else if(autoCreate) {
            return create(player);
        }
        return null;
    }

    public User create(Player player){
        User user = get(player, false);
        if(user == null){
            user = new User(player);
            service.all().add(user);
        }
        Base.captchaController.build(user);
        return user;
    }

    public UserService getService() {
        return service;
    }

}