package br.com.frach.bolsa.controller;

import br.com.frach.bolsa.data.Bolsa;
import br.com.frach.bolsa.service.BolsaService;

import java.util.Optional;

public class BolsaController {

    private BolsaService service;

    public BolsaController(BolsaService service){
        this.service = service;
    }

    public double apply(double value){
        Optional<Bolsa> optionalBolsa = service.get("Bolsa");
        if(!optionalBolsa.isPresent()){
            return value;
        }
        Bolsa bolsa = optionalBolsa.get();
        double result = value * bolsa.getPercent();
        result = result / 100;
        return result;
    }

}