package br.com.frach.bolsa.controller;

import br.com.frach.bolsa.data.User;
import br.com.frach.bolsa.data.UserCaptcha;
import br.com.frach.bolsa.service.CaptchaService;

import java.util.Optional;

public class CaptchaController {

    private CaptchaService service;

    public CaptchaController(CaptchaService service){
        this.service = service;
    }

    public void build(User user){
        Optional<UserCaptcha> optionalUserCaptcha = service.get(user);
        if(!optionalUserCaptcha.isPresent()){
            UserCaptcha userCaptcha = new UserCaptcha(user);
            userCaptcha.setInCaptcha(false);
            userCaptcha.setRequire(true);
            service.all().add(userCaptcha);
        }
    }

    public CaptchaService getService() {
        return service;
    }

}