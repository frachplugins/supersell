package br.com.frach.bolsa.controller;

import br.com.frach.bolsa.Base;
import br.com.frach.bolsa.data.*;
import br.com.frach.bolsa.service.ShopService;
import br.com.frach.bolsa.utils.MessageBuilder;
import br.com.frach.bolsa.utils.NumberFormater;
import com.google.common.collect.Lists;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.Optional;

public class ShopController {

    private ShopService service;

    public ShopController(ShopService service){
        this.service = service;
    }

    public FacadeShop sell(User user){
        Optional<Shop> optionalShop = service.get(user.getGroup());

        if(!optionalShop.isPresent()){
            user.getPlayer().sendMessage(Base.mensagens.getString("Mensagens.NO_SHOP_FOUND"));
            return null;
        }

        Shop shop = optionalShop.get();

        Inventory inventory = user.getPlayer().getInventory();
        List<SellItem> toSell = Lists.newArrayList();

        for (int i = 0; i < inventory.getContents().length; i++) {
            ItemStack itemStack = inventory.getItem(i);
            double price = shop.getPrice(itemStack).orElse(-1D);
            if(price <= 0) continue;
            inventory.setItem(i, null);
            int amount = itemStack.getAmount();
            for (SellItem sellItem : toSell) {
                if(sellItem.getItem().isSimilar(itemStack)){
                    amount += sellItem.getAmount();
                    toSell.remove(sellItem);
                    break;
                }
            }
            toSell.add(new SellItem(itemStack, price, amount));
        }

        FacadeShop facadeShop = new FacadeShop(user, shop, toSell);

        if(facadeShop.getItemCount() <= 0){
            user.getPlayer().sendMessage(Base.mensagens.getString("Mensagens.NO_ITENS"));
            return null;
        }

        MessageBuilder builder = new MessageBuilder(Base.mensagens.getString("Mensagens.SELL_ITENS"));
        builder.add("${quantity}", "" + facadeShop.getItemCount());
        builder.add("${price}", NumberFormater.format(facadeShop.getTotal()));
        builder.add("${price_clean}", NumberFormater.format(facadeShop.getTotalClean()));

        user.getPlayer().sendMessage(builder.build());

        return facadeShop;
    }

    public ShopService getService() {
        return service;
    }

}