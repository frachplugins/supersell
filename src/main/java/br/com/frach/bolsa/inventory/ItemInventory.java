package br.com.frach.bolsa.inventory;

import br.com.frach.bolsa.Base;
import br.com.frach.bolsa.data.Shop;
import br.com.frach.bolsa.data.ShopItem;
import br.com.frach.bolsa.data.User;
import br.com.frach.bolsa.utils.ItemStackBuilder;
import br.com.frach.bolsa.utils.NumberFormater;
import br.com.frach.bolsa.utils.inventory.ClickableItem;
import br.com.frach.bolsa.utils.inventory.SmartInventory;
import br.com.frach.bolsa.utils.inventory.content.InventoryContents;
import br.com.frach.bolsa.utils.inventory.content.InventoryProvider;
import br.com.frach.bolsa.utils.inventory.content.Pagination;
import br.com.frach.bolsa.utils.inventory.content.SlotIterator;
import com.google.common.collect.Lists;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Optional;

public class ItemInventory implements InventoryProvider {

    @Override
    public void init(Player player, InventoryContents contents) {
        enable(player, contents);
    }

    @Override
    public void update(Player player, InventoryContents contents) { }

    private void enable(Player player, InventoryContents contents){
        User user = Base.userController.get(player, true);
        Optional<Shop> optionalShop = Base.shopController.getService().get(user.getGroup());
        if(!optionalShop.isPresent()) {
            player.sendMessage(Base.mensagens.getString("Mensagens.NO_SHOP_FOUND"));
            player.closeInventory();
            return;
        }
        Shop shop = optionalShop.get();

        Pagination pagination = contents.pagination();
        List<ShopItem> shopItens = shop.getItens();
        ClickableItem[] items = new ClickableItem[shopItens.size()];

        for (int i = 0; i < items.length; i++) {

            ItemStackBuilder itemStackBuilder = new ItemStackBuilder(shopItens.get(i).getItem());

            List<String> lore = Lists.newArrayList();

            for (String s : Base.gui.getStringList("Menu." + shop.getName() + ".Item")) {
                lore.add(s.replace("${price}", NumberFormater.format(shopItens.get(i).getPrice())));
            }

            itemStackBuilder.setLore(lore);

            items[i] = ClickableItem.empty(itemStackBuilder.build());

        }

        pagination.setItemsPerPage(14);
        pagination.setItems(items);

        SlotIterator slotIterator = contents.newIterator(SlotIterator.Type.HORIZONTAL, 2, 1);
        slotIterator.blacklist(2, 0);
        slotIterator.blacklist(3, 0);
        slotIterator.blacklist(2, 8);
        slotIterator.blacklist(3, 8);

        pagination.addToIterator(slotIterator);

    }

    public static void open(Player player){
        SmartInventory.builder().size(5, 9).title("§8Lista de itens").provider(new ItemInventory()).build().open(player, 0);
    }

}