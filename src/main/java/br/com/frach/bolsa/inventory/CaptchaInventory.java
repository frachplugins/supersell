package br.com.frach.bolsa.inventory;

import br.com.frach.bolsa.Base;
import br.com.frach.bolsa.data.User;
import br.com.frach.bolsa.data.UserCaptcha;
import br.com.frach.bolsa.utils.CaptchaUtil;
import br.com.frach.bolsa.utils.ItemStackBuilder;
import br.com.frach.bolsa.utils.inventory.ClickableItem;
import br.com.frach.bolsa.utils.inventory.SmartInventory;
import br.com.frach.bolsa.utils.inventory.content.InventoryContents;
import br.com.frach.bolsa.utils.inventory.content.InventoryProvider;
import br.com.frach.bolsa.utils.inventory.content.SlotPos;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Optional;

public class CaptchaInventory implements InventoryProvider {

    @Override
    public void init(Player player, InventoryContents contents) {

        contents.setProperty("countdown", System.currentTimeMillis());

        User user = Base.userController.get(player, true);

        Optional<UserCaptcha> optionalUserCaptcha = Base.captchaController.getService().get(user);

        if(!optionalUserCaptcha.isPresent()){
            player.sendMessage(ChatColor.RED + "Ocorreu um erro ao setar seu captcha.");
            player.closeInventory();
            return;
        }

        UserCaptcha userCaptcha = optionalUserCaptcha.get();

        contents.set(new SlotPos(1, 1), ClickableItem.of(new ItemStackBuilder(CaptchaUtil.CaptchaItem.SWORD.getMaterial()).setName(ChatColor.GOLD + CaptchaUtil.CaptchaItem.SWORD.getName()).build(), e->{
            solveCaptcha(userCaptcha, user, e.getCurrentItem(), false);
        }));

        contents.set(new SlotPos(1, 3), ClickableItem.of(new ItemStackBuilder(CaptchaUtil.CaptchaItem.JUKEBOX.getMaterial()).setName(ChatColor.GOLD + CaptchaUtil.CaptchaItem.JUKEBOX.getName()).build(), e->{
            solveCaptcha(userCaptcha, user, e.getCurrentItem(), false);
        }));

        contents.set(new SlotPos(1, 5), ClickableItem.of(new ItemStackBuilder(CaptchaUtil.CaptchaItem.FISH.getMaterial()).setName(ChatColor.GOLD + CaptchaUtil.CaptchaItem.FISH.getName()).build(), e->{
            solveCaptcha(userCaptcha, user, e.getCurrentItem(), false);
        }));

        contents.set(new SlotPos(1, 7), ClickableItem.of(new ItemStackBuilder(CaptchaUtil.CaptchaItem.BOOK.getMaterial()).setName(ChatColor.GOLD + CaptchaUtil.CaptchaItem.BOOK.getName()).build(), e->{
            solveCaptcha(userCaptcha, user, e.getCurrentItem(), false);
        }));

    }

    private void solveCaptcha(UserCaptcha userCaptcha, User user, ItemStack currentItem, boolean timeout){

        if(!timeout) {
            if (currentItem != null && currentItem.getType().equals(userCaptcha.getMaterial().getMaterial())) {
                userCaptcha.setInCaptcha(false);
                userCaptcha.setRequire(false);
                userCaptcha.setMaterial(null);

                user.getPlayer().sendMessage(ChatColor.GREEN + "Captcha validado com sucesso. Vender habilitado.");

                user.getPlayer().closeInventory();
            } else {
                userCaptcha.setRequire(true);
                userCaptcha.setInCaptcha(false);

                user.setAutoSell(false);
                user.setShiftSell(false);

                user.getPlayer().sendMessage(ChatColor.RED + "Captcha invalido, tente novamente.");
                user.getPlayer().closeInventory();
            }
        } else {
            userCaptcha.setRequire(true);
            userCaptcha.setInCaptcha(false);

            user.setAutoSell(false);
            user.setShiftSell(false);

            user.getPlayer().sendMessage(ChatColor.RED + "Tempo limite do captcha atingido, tente novamente.");
            user.getPlayer().closeInventory();
        }
    }

    @Override
    public void update(Player player, InventoryContents contents) {

        long time = contents.property("countdown");

        int seconds = (int) (System.currentTimeMillis() - time) / 1000;

        if(seconds >= 30){

            User user = Base.userController.get(player, true);

            Optional<UserCaptcha> optionalUserCaptcha = Base.captchaController.getService().get(user);

            if(optionalUserCaptcha.isPresent()) {
                solveCaptcha(optionalUserCaptcha.get(), user, null, true);
            }

        }

    }

    public static void open(Player player){

        User user = Base.userController.get(player, true);

        Optional<UserCaptcha> optionalUserCaptcha = Base.captchaController.getService().get(user);

        if(optionalUserCaptcha.isPresent()){

            UserCaptcha userCaptcha = optionalUserCaptcha.get();

            userCaptcha.setMaterial( CaptchaUtil.CaptchaItem.random() );

            SmartInventory.builder().provider(new CaptchaInventory()).title(ChatColor.DARK_GRAY + "Clique no(a) " + ChatColor.YELLOW + userCaptcha.getMaterial().getName()).size(3, 9).id("captcha-" + player.getName()).build().open(player);
        }

    }

}