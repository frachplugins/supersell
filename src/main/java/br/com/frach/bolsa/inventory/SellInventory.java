package br.com.frach.bolsa.inventory;

import br.com.frach.bolsa.Base;
import br.com.frach.bolsa.data.Shop;
import br.com.frach.bolsa.data.User;
import br.com.frach.bolsa.data.UserCaptcha;
import br.com.frach.bolsa.utils.ItemStackBuilder;
import br.com.frach.bolsa.utils.inventory.ClickableItem;
import br.com.frach.bolsa.utils.inventory.SmartInventory;
import br.com.frach.bolsa.utils.inventory.content.*;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Optional;

public class SellInventory implements InventoryProvider {

    @Override
    public void init(Player player, InventoryContents contents) {
        this.enable(player, contents);
    }

    @Override
    public void update(Player player, InventoryContents contents) {
        int state = contents.property("state", 0);
        contents.setProperty("state", state + 1);
        if(state % 5 != 0)
            return;
        enable(player, contents);
    }

    private void enable(Player player, InventoryContents contents){

        final User user = Base.userController.get(player, true);

        contents.set(new SlotPos(1, 1), ClickableItem.of(new ItemStackBuilder(Material.SIGN).setName("§6Vender agora").setGlow(true).build(), e->{
            Base.shopController.sell(user);
        }));

        contents.set(new SlotPos(1, 2), ClickableItem.of(new ItemStackBuilder(Material.BOOK_AND_QUILL)
                .setName("§bLista de vendas").addLore(
                        "", "§7Clicando nessa opção poderá,", "§7ver preços de §avenda §7do §erank §7atual.", "",
                        "§7Os §avalores §7listados estão em §e2304x§7,",
                        "§7ou seja os §avalroes §7esta por inventário.",
                        "", "§aEssa opção, está disponível para §3todos."
                ).build(), e-> ItemInventory.open(player)));

        contents.set(new SlotPos(1, 6), ClickableItem.of(new ItemStackBuilder(Material.INK_SACK).setName(user.isShiftSell() ? "§7ShiftSell §aAtivado" : "§7ShiftSell §cDesativado" ).setDurability(user.isShiftSell() ?  10 : 8 ).build(), e-> user.toggleShiftSell() ));
        contents.set(new SlotPos(1, 7), ClickableItem.of(new ItemStackBuilder(Material.INK_SACK).setName(user.isAutoSell() ? "§7AutoSell §aAtivado" : "§7AutoSell §cDesativado" ).setDurability(user.isAutoSell() ?  10 : 8 ).build(), e-> user.toggleAutoSell() ));

    }

    public static void open(Player player){
        SmartInventory.builder().provider(new SellInventory()).size(3, 9).title("§8Menu de Vendas").build().open(player, 0);
    }

}