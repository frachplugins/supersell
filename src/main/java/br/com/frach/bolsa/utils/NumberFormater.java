package br.com.frach.bolsa.utils;

import java.text.DecimalFormat;

public class NumberFormater {

    private static String[] _NUMBER_SUFFIX = {
            "C", "K", "M", "B", "T", "Q", "QI", "S", "SEP", "OC", "N", "DEC", "UN", "DUO", "TRE"
    };

    public static String format(double value){
        int index;
        for(index = 0; value / 1000.0 >= 1.0; value /= 1000.0, ++index){}
        final DecimalFormat decimalFormat = new DecimalFormat("#.##");
        return String.format("%s %s", decimalFormat.format(value), _NUMBER_SUFFIX[index]);
    }

}