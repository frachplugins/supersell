package br.com.frach.bolsa.utils;

import java.io.*;
import org.bukkit.inventory.meta.*;
import org.bukkit.enchantments.*;
import org.bukkit.craftbukkit.v1_8_R3.inventory.*;
import org.bukkit.entity.*;
import org.bukkit.*;
import org.bukkit.inventory.*;
import java.util.stream.*;
import java.util.*;
import org.bukkit.event.inventory.*;
import org.bukkit.event.*;

public class ItemStackBuilder implements Serializable
{
    private ItemStack item;
    private ItemMeta meta;
    private EnchantmentStorageMeta storage;
    private List<String> lore;
    private boolean glow;
    private net.minecraft.server.v1_8_R3.ItemStack item2;
    private SkullMeta skullMeta;
    private static HashMap<ItemStack, InventoryAction> inventoryActions;
    
    public ItemStackBuilder(final ItemStack item) {
        this.glow = false;
        this.item = item;
        if (item.getType() == Material.ENCHANTED_BOOK) {
            this.storage = (EnchantmentStorageMeta)item.getItemMeta();
            this.lore = ((this.storage != null && this.storage.hasLore()) ? this.storage.getLore() : new ArrayList<String>());
        }
        else if (item.getType() == Material.SKULL_ITEM || item.getType() == Material.SKULL) {
            this.skullMeta = (SkullMeta)item.getItemMeta();
            this.lore = ((this.skullMeta != null && this.skullMeta.hasLore()) ? this.skullMeta.getLore() : new ArrayList<String>());
        }
        else {
            this.meta = item.getItemMeta();
            this.lore = ((this.meta != null && this.meta.hasLore()) ? this.meta.getLore() : new ArrayList<String>());
        }
    }
    
    public ItemStackBuilder(final Material material) {
        this(new ItemStack(material));
    }
    
    public ItemStackBuilder setLore(final List<String> lore) {
        this.lore = lore;
        return this;
    }
    
    public ItemStackBuilder setType(final Material type) {
        this.item.setType(type);
        return this;
    }
    
    public ItemStackBuilder setOwner(final String owner) {
        if (this.item.getType() == Material.SKULL_ITEM || this.item.getType() == Material.SKULL) {
            this.skullMeta.setOwner(owner);
            return this;
        }
        return this;
    }
    
    public ItemStackBuilder setName(final String name) {
        if (this.item.getType() == Material.ENCHANTED_BOOK) {
            this.storage.setDisplayName(name);
            return this;
        }
        if (this.item.getType() == Material.SKULL_ITEM || this.item.getType() == Material.SKULL) {
            this.skullMeta.setDisplayName(name);
            return this;
        }
        this.meta.setDisplayName(name);
        return this;
    }
    
    public ItemStackBuilder addLore(final String... l) {
        for (final String x : l) {
            this.lore.add(x);
        }
        return this;
    }
    
    public ItemStackBuilder addLoreList(final List<String> l) {
        for (final String s : l) {
            this.lore.add(s.replace("&", "\ufffd"));
        }
        return this;
    }
    
    public ItemStackBuilder addStoredEnchantment(final Enchantment e, final int level) {
        this.storage.addStoredEnchant(e, level, true);
        return this;
    }
    
    public ItemStackBuilder addEnchantment(final Enchantment e, final int level) {
        this.meta.addEnchant(e, level, true);
        return this;
    }
    
    public ItemStackBuilder setDurability(final int durability) {
        this.item.setDurability((short)durability);
        return this;
    }
    
    public ItemStackBuilder setAmount(final int amount) {
        this.item.setAmount(amount);
        return this;
    }
    
    public ItemStackBuilder clearLore() {
        this.lore = new ArrayList<String>();
        return this;
    }
    
    public ItemStackBuilder replaceLore(final String oldLore, final String newLore) {
        for (int i = 0; i < this.lore.size(); ++i) {
            if (this.lore.get(i).contains(oldLore)) {
                this.lore.remove(i);
                this.lore.add(i, newLore);
                break;
            }
        }
        return this;
    }
    
    public ItemStack build() {
        if (this.item.getType() == Material.ENCHANTED_BOOK) {
            if (!this.lore.isEmpty()) {
                this.storage.setLore((List)this.lore);
                this.lore.clear();
            }
            this.item.setItemMeta((ItemMeta)this.storage);
            return this.item;
        }
        if (this.item.getType() == Material.SKULL || this.item.getType() == Material.SKULL_ITEM) {
            if (!this.lore.isEmpty()) {
                this.skullMeta.setLore((List)this.lore);
                this.lore.clear();
            }
            this.item.setItemMeta((ItemMeta)this.skullMeta);
            return this.item;
        }
        if (!this.lore.isEmpty()) {
            this.meta.setLore((List)this.lore);
            this.lore.clear();
        }
        this.item.setItemMeta(this.meta);
        if (this.glow) {
            this.setGlow(this.glow);
            return (ItemStack)CraftItemStack.asCraftMirror(this.item2);
        }
        return this.item;
    }
    
    public int getAmount() {
        return this.item.getAmount();
    }
    
    public ItemStackBuilder addInventoryAction(final Runnable runnable, final boolean cancelEvent, final Player player, final Inventory inventory) {
        ItemStackBuilder.inventoryActions.put(this.build(), new InventoryAction("" + ItemStackBuilder.inventoryActions.size() + 1, runnable, this.build(), player, cancelEvent, inventory));
        return this;
    }
    
    public ItemStackBuilder setGlow(final boolean glow) {
        if (glow) {
            this.addEnchantment(Enchantment.DURABILITY, 1);
            this.addFlag(ItemFlag.HIDE_ENCHANTS);
        }
        else {
            this.removeFlag(ItemFlag.HIDE_ENCHANTS);
        }
        return this;
    }
    
    public ItemStackBuilder addFlag(final ItemFlag... flags) {
        if (this.item.getType() == Material.ENCHANTED_BOOK) {
            this.storage.addItemFlags(flags);
        }
        else if (this.item.getType() == Material.SKULL_ITEM || this.item.getType() == Material.SKULL) {
            this.skullMeta.addItemFlags(flags);
        }
        else {
            this.meta.addItemFlags(flags);
        }
        return this;
    }
    
    public ItemStackBuilder removeFlag(final ItemFlag... flags) {
        if (this.item.getType() == Material.ENCHANTED_BOOK) {
            this.storage.removeItemFlags(flags);
        }
        else if (this.item.getType() == Material.SKULL_ITEM || this.item.getType() == Material.SKULL) {
            this.skullMeta.removeItemFlags(flags);
        }
        else {
            this.meta.removeItemFlags(flags);
        }
        return this;
    }
    
    static {
        ItemStackBuilder.inventoryActions = new HashMap<ItemStack, InventoryAction>();
    }
    
    public static class InventoryBuilder
    {
        private String name;
        private HashMap<Integer, HashMap<Integer, ItemStackBuilder>> itens;
        private HashMap<Integer, Inventory> paginas;
        
        public InventoryBuilder(final String name, final Integer slot) {
            this.setName(name);
            this.itens = new HashMap<Integer, HashMap<Integer, ItemStackBuilder>>() {
                private static final long serialVersionUID = 1L;
                
                {
                    this.put(0, new HashMap<Integer, ItemStackBuilder>());
                }
            };
            this.paginas = new HashMap<Integer, Inventory>() {
                private static final long serialVersionUID = 1L;
                
                {
                    this.put(0, Bukkit.createInventory((InventoryHolder)new ItemStackBuilderInventoryHolder(), (int)slot, InventoryBuilder.this.getName()));
                }
            };
        }
        
        public void setItem(final int pagina, final int slot, final ItemStackBuilder item) {
            this.itens.get(pagina).put(slot, item);
        }
        
        public void removeItem(final int pagina, final int slot) {
            this.itens.get(pagina).remove(slot);
        }
        
        public List<ItemStackBuilder> getItens(final int pagina) {
            return this.itens.get(pagina).values().stream().collect(Collectors.toList());
        }
        
        public void addInventory(final String name, final int slot) {
            this.paginas.put(this.paginas.size(), Bukkit.createInventory((InventoryHolder)new ItemStackBuilderInventoryHolder(), slot, name));
            this.itens.put(this.itens.size(), new HashMap<Integer, ItemStackBuilder>());
        }
        
        public void openInventory(final Player player, final int pagina) {
            this.itens.get(pagina).entrySet().forEach(item -> this.paginas.get(pagina).setItem((int)item.getKey(), ((ItemStackBuilder)item.getValue()).build()));
            player.openInventory((Inventory)this.paginas.get(pagina));
        }
        
        public Inventory getInventory(final int pagina) {
            return this.paginas.get(pagina);
        }
        
        public String getName() {
            return this.name;
        }
        
        public void setName(final String name) {
            this.name = name;
        }
    }
    
    public class InventoryAction
    {
        private String actionId;
        private Runnable runnable;
        private ItemStack item;
        private Player player;
        private boolean cancelEvent;
        private Inventory inventory;
        
        public InventoryAction(final String actionId, final Runnable runnable, final ItemStack item, final Player player, final boolean cancelEvent, final Inventory inventory) {
            this.actionId = actionId;
            this.runnable = runnable;
            this.item = item;
            this.player = player;
            this.cancelEvent = cancelEvent;
            this.setInventory(inventory);
        }
        
        public Player getPlayer() {
            return this.player;
        }
        
        public void setPlayer(final Player player) {
            this.player = player;
        }
        
        public boolean isCancelEvent() {
            return this.cancelEvent;
        }
        
        public void setCancelEvent(final boolean cancelEvent) {
            this.cancelEvent = cancelEvent;
        }
        
        public String getActionId() {
            return this.actionId;
        }
        
        public void setActionId(final String actionId) {
            this.actionId = actionId;
        }
        
        public Runnable getRunnable() {
            return this.runnable;
        }
        
        public void setRunnable(final Runnable runnable) {
            this.runnable = runnable;
        }
        
        public ItemStack getItem() {
            return this.item;
        }
        
        public void setItem(final ItemStack item) {
            this.item = item;
        }
        
        public Inventory getInventory() {
            return this.inventory;
        }
        
        public void setInventory(final Inventory inventory) {
            this.inventory = inventory;
        }
    }
    
    public static class ItemStackBuilderEvents implements Listener
    {
        @EventHandler
        public void inventoryClick(final InventoryClickEvent e) {
            final Player player = (Player)e.getWhoClicked();
            final ItemStack currentItem = e.getCurrentItem();
            if (currentItem != null && currentItem.getType() != Material.AIR && e.getInventory().getHolder() instanceof ItemStackBuilderInventoryHolder && ItemStackBuilder.inventoryActions.get(currentItem) != null) {
                final InventoryAction action = ItemStackBuilder.inventoryActions.get(currentItem);
                if (action.getInventory().equals(action.getInventory()) && action.getPlayer().equals(player)) {
                    e.setCancelled(action.isCancelEvent());
                    action.getRunnable().run();
                }
            }
        }
        
        public static void clearData() {
            ItemStackBuilder.inventoryActions.clear();
        }
    }
    
    public static class ItemStackBuilderInventoryHolder implements InventoryHolder
    {
        public Inventory getInventory() {
            return null;
        }
    }
}
