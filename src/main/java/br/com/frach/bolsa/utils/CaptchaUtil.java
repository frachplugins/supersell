package br.com.frach.bolsa.utils;

import org.bukkit.Material;

import java.util.Random;

public class CaptchaUtil {

    public enum CaptchaItem {

        FISH(Material.RAW_FISH, "Peixe"), JUKEBOX(Material.JUKEBOX, "Jukebox"), BOOK(Material.BOOK, "Livro"), SWORD(Material.DIAMOND_SWORD, "Espada");

        private Material material;
        private String name;

        CaptchaItem(Material material, String name){
            this.material = material;
            this.name = name;
        }

        public Material getMaterial() {
            return material;
        }

        public String getName() {
            return name;
        }

        public static CaptchaItem random(){
            return CaptchaItem.values()[new Random().nextInt(CaptchaItem.values().length)];
        }

    }

}