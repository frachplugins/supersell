package br.com.frach.bolsa.utils;

import com.google.common.collect.Maps;

import java.util.HashMap;

public class MessageBuilder {

    private HashMap<String, String> placeholders = Maps.newHashMap();

    private String message;

    public MessageBuilder(String message){
        this.message = message;
    }

    public void add(String key, String value){
        this.placeholders.put(key, value);
    }

    public String build(){
        placeholders.forEach((key, value) -> message = message.replace(key, value));
        return message;
    }

}
