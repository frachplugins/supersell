package br.com.frach.bolsa.utils;

import com.google.common.collect.Lists;
import org.bukkit.entity.EntityType;
import org.bukkit.plugin.*;
import org.bukkit.configuration.file.*;
import java.io.*;
import org.bukkit.configuration.*;
import org.bukkit.enchantments.*;
import org.bukkit.inventory.*;
import org.bukkit.inventory.meta.*;
import org.bukkit.*;
import java.util.*;

public class PiranhaConfig
{
    private File file;
    private FileConfiguration fileConfiguration;
    String fileName;
    
    public PiranhaConfig(final Plugin plugin, String fileName) {
        this.fileName = fileName;
        if (!plugin.getDataFolder().exists()) {
            plugin.getDataFolder().mkdirs();
        }
        if (!fileName.isEmpty()) {
            fileName = (fileName.endsWith(".yml") ? fileName : (fileName + ".yml"));
        }
        this.file = new File(plugin.getDataFolder(), fileName.isEmpty() ? "config.yml" : fileName);
        if (!this.file.exists()) {
            plugin.saveResource(fileName, false);
        }
        this.fileConfiguration = (FileConfiguration)YamlConfiguration.loadConfiguration(this.file);
    }
    
    public void createNewFile() throws IOException {
        this.file.createNewFile();
    }
    
    public Object get(final String path) {
        return this.fileConfiguration.get(path);
    }
    
    public String getString(final String path) {
        return ChatColor.translateAlternateColorCodes('&', this.fileConfiguration.getString(path));
    }
    
    public int getInt(final String path) {
        return this.fileConfiguration.getInt(path);
    }
    
    public List<?> getList(final String path) {
        return (List<?>)this.fileConfiguration.getList(path);
    }
    
    public List<String> getStringList(final String path) {
        List<String> list = Lists.newArrayList();
        for (String s : this.fileConfiguration.getStringList(path)) {
            list.add(ChatColor.translateAlternateColorCodes('&', s));
        }
        return list;
    }

    public EntityType getEntityType(final String path){
        return EntityType.valueOf(getString(path));
    }
    
    public double getDouble(final String path) {
        return this.fileConfiguration.getDouble(path);
    }
    
    public float getFloat(final String path) {
        return (float)this.fileConfiguration.getDouble(path);
    }
    
    public List<Integer> getIntegetList(final String path) {
        return (List<Integer>)this.fileConfiguration.getIntegerList(path);
    }
    
    public List<Double> getDoubleList(final String path) {
        return (List<Double>)this.fileConfiguration.getDoubleList(path);
    }
    
    public List<Float> getFloatList(final String path) {
        return (List<Float>)this.fileConfiguration.getFloatList(path);
    }
    
    public boolean getBoolean(final String path) {
        return this.fileConfiguration.getBoolean(path);
    }
    
    public void set(final String path, final Object value) {
        this.fileConfiguration.set(path, value);
    }
    
    public ConfigurationSection getConfigurationSection(final String path) {
        return this.fileConfiguration.getConfigurationSection(path);
    }
    
    public boolean contains(final String path) {
        return this.get(path) != null;
    }
    
    public void setItemStack(final String path, final ItemStack item) {
        final ItemMeta meta = item.getItemMeta();
        final String itemId = item.getType() + ":" + item.getDurability();
        final int amount = item.getAmount();
        String display_name = item.getType().name();
        List<String> lore = new ArrayList<String>();
        final Set<ItemFlag> itemFlag = (Set<ItemFlag>)meta.getItemFlags();
        if (meta != null) {
            display_name = (meta.hasDisplayName() ? meta.getDisplayName() : display_name);
            lore = (meta.hasLore() ? meta.getLore() : lore);
        }
        final Map<Enchantment, Integer> enchantments = (Map<Enchantment, Integer>)item.getEnchantments();
        final List<String> encantamentos = new ArrayList<String>() {
            private static final long serialVersionUID = 1L;
            
            {
                enchantments.entrySet().forEach(encantamento -> this.add(encantamento.getKey().getName() + "," + encantamento.getValue()));
            }
        };
        final List<String> flags = new ArrayList<String>() {
            private static final long serialVersionUID = 1L;
            
            {
                itemFlag.forEach(flag -> this.add(flag.name()));
            }
        };
        this.set(path + ".Item", itemId);
        this.set(path + ".Amount", amount);
        this.set(path + ".DisplayName", display_name);
        this.set(path + ".Lore", lore);
        this.set(path + ".Encantamentos", encantamentos);
        this.set(path + ".ItemFlag", flags);
        this.save();
    }
    
    public ItemStack getItemStack(final String path) {
        if (!this.contains(path)) {
            return null;
        }
        final String itemId = this.getString(path + ".Item");
        final String[] item_id = itemId.split(":");
        final int amount = this.getInt(path + ".Amount");
        final String display_name = this.getString(path + ".DisplayName");
        final List<String> lore = this.getStringList(path + ".Lore");
        final List<String> encantamentos = this.getStringList(path + ".Encantamentos");
        final List<String> flags = this.getStringList(path + ".ItemFlag");
        final ItemStackBuilder item = new ItemStackBuilder(Material.getMaterial(item_id[0]));
        item.setAmount(amount);
        item.setName(ChatColor.translateAlternateColorCodes('&', display_name));
        item.setDurability(Integer.valueOf(item_id[1]));
        lore.forEach(l -> item.addLore(ChatColor.translateAlternateColorCodes('&', l)));
        encantamentos.forEach(encantamento -> item.addEnchantment(Enchantment.getByName(encantamento.split(",")[0]), Integer.valueOf(encantamento.split(",")[1])));
        flags.forEach(flag -> {
            if(flag.equalsIgnoreCase("glow")){
                item.setGlow(true);
            } else {
                item.addFlag(ItemFlag.valueOf(flag));
            }
        });
        return item.build();
    }
    
    public List<ItemStack> getItemStackSection(final String path) {
        final List<ItemStack> itens = new ArrayList<ItemStack>();
        final ConfigurationSection configurationSection = this.getConfigurationSection(path);
        if (configurationSection == null) {
            return itens;
        }
        for (final String section : configurationSection.getKeys(false)) {
            itens.add(this.getItemStack(path + "." + section));
        }
        return itens;
    }
    
    public void save() {
        try {
            this.fileConfiguration.save(this.file);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
