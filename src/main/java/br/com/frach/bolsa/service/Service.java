package br.com.frach.bolsa.service;

import java.util.List;
import java.util.Optional;

public interface Service<T, K>  {

    List<T> all();
    Optional<T> get(K k);

    void init();
    void stop();
    void run();

}