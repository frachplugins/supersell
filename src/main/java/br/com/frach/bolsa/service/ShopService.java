package br.com.frach.bolsa.service;

import br.com.frach.bolsa.Base;
import br.com.frach.bolsa.data.Shop;
import br.com.frach.bolsa.data.ShopItem;
import br.com.frach.bolsa.data.ShopMultiplier;
import com.google.common.collect.Lists;
import org.bukkit.configuration.ConfigurationSection;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ShopService implements Service<Shop, String> {

    private List<Shop> shops = Lists.newArrayList();
    private List<ShopMultiplier> multipliers = Lists.newArrayList();

    @Override
    public List<Shop> all() {
        return shops.stream().sorted(Comparator.comparing(Shop::getName)).collect(Collectors.toList());
    }

    public List<ShopMultiplier> getMultipliers(){
        return multipliers;
    }

    @Override
    public Optional<Shop> get(String name) {
        Optional<Shop> shop = shops.stream().filter(s -> s.getAliases().contains(name)).findFirst();
        System.out.println(shop);
        if(shop.isPresent()){
            return shop;
        }
        return Optional.empty();
    }

    @Override
    public void init() {

        ConfigurationSection multiplierSection = Base.config.getConfigurationSection("Multiplicadores");
        if(multiplierSection != null) {
            for (String key : multiplierSection.getKeys(false)) {
                ShopMultiplier multiplier = new ShopMultiplier(key, multiplierSection.getDouble(key));
                multipliers.add(multiplier);
            }
        }

        ConfigurationSection section = Base.config.getConfigurationSection("Shop");
        if(section == null){
            return;
        }
        for (String key : section.getKeys(false)) {
            List<ShopItem> itens = Lists.newArrayList();
            for (String itemKey : section.getConfigurationSection(key + ".Itens").getKeys(false)) {
                itens.add(new ShopItem(
                        Base.config.getItemStack("Shop." + key + ".Itens." + itemKey),
                        section.getDouble(key + ".Itens." + itemKey + ".Price")
                ));
            }
            List<String> aliases = section.getStringList(key + ".Aliases");
            shops.add(new Shop(key, itens, aliases));
        }
    }

    @Override
    public void stop() {
        shops.clear();
    }

    @Override
    public void run() {
        // some daqui, playboy filho da mãe.
    }

}