package br.com.frach.bolsa.service;

import br.com.frach.bolsa.Base;
import br.com.frach.bolsa.data.Bolsa;
import com.google.common.collect.Lists;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;
import java.util.Optional;

public class BolsaService implements Service<Bolsa, String> {

    private Bolsa bolsa;

    @Override
    public List<Bolsa> all() {
        return Lists.newArrayList(bolsa);
    }

    /**
     * THIS PARAM IS NOT REQUIRED TO RETURN 'BOLSA' VARIABLE
     * @param s NOT REQUIRED
     * @return Optional#Bolsa
     */
    @Override
    public Optional<Bolsa> get(String s) {
        return Optional.of(bolsa);
    }

    @Override
    public void init() {
        bolsa = new Bolsa("Bolsa", 100, Base.bolsas.getInt("Bolsa.Delay"));

        new BukkitRunnable(){
            @Override
            public void run() {
                BolsaService.this.run();
            }
        }.runTaskTimer(Base.getPlugin(Base.class), 20L, 20L * bolsa.getDelay());

    }

    @Override
    public void stop() {

    }

    @Override
    public void run() {
        bolsa.update();
    }

}