package br.com.frach.bolsa.service;

import br.com.frach.bolsa.Base;
import br.com.frach.bolsa.data.User;
import br.com.frach.bolsa.data.UserCaptcha;
import com.google.common.collect.Lists;

import java.util.List;
import java.util.Optional;

public class CaptchaService implements Service<UserCaptcha, User> {

    private List<UserCaptcha> captchas = Lists.newArrayList();

    @Override
    public List<UserCaptcha> all() {
        return captchas;
    }

    @Override
    public Optional<UserCaptcha> get(User user) {
        Optional<UserCaptcha> optionalUserCaptcha = captchas.stream().filter(u -> u.getUser().equals(user)).findFirst();
        if(optionalUserCaptcha.isPresent()){
            return optionalUserCaptcha;
        }
        return Optional.empty();
    }

    @Override
    public void init() {
        for (User user : Base.userController.getService().all()) {
            UserCaptcha userCaptcha = new UserCaptcha(user);
            userCaptcha.setInCaptcha(false);
            userCaptcha.setRequire(true);
            captchas.add(userCaptcha);
        }
    }

    @Override
    public void stop() {
        captchas.clear();
    }

    @Override
    public void run() {
        captchas.forEach(UserCaptcha::run);
    }

}