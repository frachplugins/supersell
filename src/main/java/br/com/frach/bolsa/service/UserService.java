package br.com.frach.bolsa.service;

import br.com.frach.bolsa.data.User;
import com.google.common.collect.Lists;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Optional;

public class UserService implements Service<User, Player> {

    private List<User> users = Lists.newArrayList();

    @Override
    public List<User> all() {
        return users;
    }

    @Override
    public Optional<User> get(Player player) {
        Optional<User> user = users.stream().filter(p -> p.getPlayer().equals(player)).findFirst();
        if(user.isPresent()){
            return user;
        }
        return Optional.empty();
    }

    @Override
    public void init() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            users.add(new User(player));
        }
    }

    @Override
    public void stop() {
        users.clear();
    }

    @Override
    public void run() {

    }

}