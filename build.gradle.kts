import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.3.10"
    id("com.github.johnrengelman.shadow") version "2.0.3"
    id("net.minecrell.plugin-yml.bukkit") version "0.2.1"
}

group = "br.com.frach.bolsa.Base"
version = "1.0-SNAPSHOT"

repositories {
    jcenter()
    mavenLocal()

    // spigot
    maven {
        name = "spigot"
        url = uri("https://hub.spigotmc.org/nexus/content/repositories/snapshots/")
    }
    maven {
        name = "sonatype"
        url = uri("https://oss.sonatype.org/content/repositories/snapshots/")
    }

    // exposed
    maven {
        name = "exposed"
        url = uri("https://dl.bintray.com/kotlin/exposed")
    }
}

dependencies {
    compile(kotlin("stdlib"))
    compile(kotlin("reflect"))

    compile("org.jetbrains.exposed:exposed:0.11.2")

    // dependecias nao compiladas dentro
    compileOnly(files(File(projectDir, "libs/spigot-1.8.8.jar")))
    compileOnly(files(File(projectDir, "libs/Vault.jar")))
    compileOnly(files(File(projectDir, "libs/PermissionsEx.jar")))
    compileOnly(files(File(projectDir, "libs/FrachProRankup.jar")))
}

tasks {
    "compileKotlin"(KotlinCompile::class) {
        kotlinOptions {
            jvmTarget = "1.8"
        }
    }
    "shadowJar"(ShadowJar::class) {
        baseName = project.name
        classifier = ""
    }

    tasks.withType<JavaCompile>{
        options.encoding = "UTF-8"
    }

}

// plugin.yml
bukkit {
    name = "FrachVender"
    version = project.version.toString()
    main = project.group.toString()

    depend = listOf("Vault", "FrachProRankup")

    commands {
        "vender"{
            description = "Comando de vender super legal <3"
        }
    }

    authors = listOf("FrachDev_")

}